## 实验1：SQL语句的执行计划分析与优化指导

葛展鹏 202010211107 20软件工程1班

### 实验目的

分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

### 实验数据库和用户

数据库是pdborcl，用户是sys和hr

### 实验内容

1. 向用户hr授予以下视图的选择权限：

   v_$sesstat, v_$statname 和 v_$session

   ```
   $ sqlplus sys/123@localhost/pdborcl as sysdba
   @$ORACLE_HOME/sqlplus/admin/plustrce.sql
   create role plustrace;
   GRANT SELECT ON v_$sesstat TO plustrace;
   GRANT SELECT ON v_$statname TO plustrace;
   GRANT SELECT ON v_$mystat TO plustrace;
   GRANT plustrace TO dba WITH ADMIN OPTION;
   GRANT plustrace TO hr;
   GRANT SELECT ON v_$sql TO hr;
   GRANT SELECT ON v_$sql_plan TO hr;
   GRANT SELECT ON v_$sql_plan_statistics_all TO hr;
   GRANT SELECT ON v_$session TO hr;
   GRANT SELECT ON v_$parameter TO hr; 
   ```

   

2. 执行两个sql语句查询两个部门('IT'和'Sales')的部门总人数和平均工资

   ```
   set autotrace on
   
   SELECT d.department_name,count(e.job_id)as "部门总人数",
   avg(e.salary)as "平均工资"
   from hr.departments d,hr.employees e
   where d.department_id = e.department_id
   and d.department_name in ('IT','Sales')
   GROUP BY d.department_name;
   
   ```

   ```
   set autotrace on
   
   SELECT d.department_name,count(e.job_id)as "部门总人数",
   avg(e.salary)as "平均工资"
   FROM hr.departments d,hr.employees e
   WHERE d.department_id = e.department_id
   GROUP BY d.department_name
   HAVING d.department_name in ('IT','Sales');
   
   ```

3. 查询结果

   ![pict1](D:\code\oracle\oracle\test1\pict1.png)

4. 通过sqldeveloper的优化指导工具进行优化指导

   ![pict3](D:\code\oracle\oracle\test1\pict3.png)

### 实验分析

经过这个实验我知道了SQL语句的执行计划的重要作用，它可以帮助我们知道SQL语句的性能帮助我们选择更好的SQL语句。

在此次实验我执行的两个中我认为第二个sql语句更好

```
SELECT d.department_name,count(e.job_id)as "部门总人数",
avg(e.salary)as "平均工资"
FROM hr.departments d,hr.employees e
WHERE d.department_id = e.department_id
GROUP BY d.department_name
HAVING d.department_name in ('IT','Sales');
```

因为根据执行计划我们发现第二个的recursive calls，consistent gets，sorts (memory)值更小。

1. recursive calls是指一个SQL查询在执行时需要多次递归调用自身，这可能会导致性能问题和死循环。

2. consistent gets越小的SQL语句通常意味着查询的性能更好。Consistent Gets是Oracle数据库中的一个性能计数器，表示从内存或磁盘中读取数据块的数量，而较少的Consistent Gets通常意味着查询的性能更好，因为它意味着减少了I/O操作的数量，从而降低了查询的响应时间。

3. Sorts (memory)越小的SQL语句通常意味着查询的性能更好。Sorts (memory)是Oracle数据库中的一个性能计数器，表示在内存中进行的排序的次数，较少的Sorts (memory)通常意味着较少的CPU时间和内存使用，从而降低了查询的响应时间。

   综上所述：第二个sql语句更好
# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计

- 学号：202010211107     姓名：葛展鹏    班级：20软件工程1班

## 实验目的

综合使用本学期的所有知识

## 实验内容

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间使用方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 实验步骤

- 第1步：表空间方案：
  - SYSTEM：系统表空间
  - USERS：用户表空间
  - SALES_DATA：用于存储销售数据的表空间

```
CREATE TABLESPACE sales_data
   DATAFILE 'sales_data.dbf'
   SIZE 100M
   AUTOEXTEND ON
   NEXT 10M;

```

![2.jpg](./1.png)

- 第2步：表方案：

  - 商品表（Products）：存储商品信息，包括商品ID、名称、价格等字段。
  - 客户表（Customers）：存储客户信息，包括客户ID、姓名、联系方式等字段。
  - 订单表（Orders）：存储订单信息，包括订单ID、客户ID、订单日期等字段。
  - 订单详情表（Order_Details）：存储订单的商品明细信息，包括订单ID、商品ID、数量等字段。

```
-- 创建表
CREATE TABLE products (
   product_id   NUMBER PRIMARY KEY,
   name         VARCHAR2(100),
   price        NUMBER
   quantity     NUMBER
);

CREATE TABLE customers (
   customer_id  NUMBER PRIMARY KEY,
   name         VARCHAR2(100),
   contact      VARCHAR2(100)
);

CREATE TABLE orders (
   order_id     NUMBER PRIMARY KEY,
   customer_id  NUMBER,
   order_date   DATE,
   CONSTRAINT fk_customer
      FOREIGN KEY (customer_id)
      REFERENCES customers(customer_id)
);

CREATE TABLE order_details (
   order_id     NUMBER,
   product_id   NUMBER,
   quantity     NUMBER,
   CONSTRAINT pk_order_details
      PRIMARY KEY (order_id, product_id),
   CONSTRAINT fk_order
      FOREIGN KEY (order_id)
      REFERENCES orders(order_id),
   CONSTRAINT fk_product
      FOREIGN KEY (product_id)
      REFERENCES products(product_id)
);
```



![2.jpg](./2.png)

- 第3步：权限及用户分配方案

  创建两个用户：SALES_USER和REPORT_USER。
  
  - SALES_USER用户：用于管理销售系统的操作，具有对商品表、客户表、订单表和订单详情表的增删改查权限。
  - REPORT_USER用户：用于生成销售报告，具有对商品表、客户表和订单表的只读权限。
  
  ```
  -- 创建用户
  CREATE USER sales_user IDENTIFIED BY password;
  CREATE USER report_user IDENTIFIED BY password;
  
  -- 分配表权限
  GRANT SELECT, INSERT, UPDATE, DELETE ON products TO sales_user;
  GRANT SELECT ON products TO report_user;
  GRANT SELECT, INSERT, UPDATE, DELETE ON customers TO sales_user;
  GRANT SELECT ON customers TO report_user;
  GRANT SELECT, INSERT, UPDATE, DELETE ON orders TO sales_user;
  GRANT SELECT ON orders TO report_user;
  GRANT SELECT, INSERT, UPDATE, DELETE ON order_details TO sales_user;
  ```
  
  ![2.jpg](./3.png)
  
- 第4步：程序包及存储过程和函数：

  创建一个名为SALES_PACKAGE的程序包，其中包含以下存储过程和函数：
  
  - 创建订单（CREATE_ORDER）：接收客户ID和商品ID列表作为参数，创建一个新的订单并插入到订单表和订单详情表中。
  - 更新订单状态（UPDATE_ORDER_STATUS）：接收订单ID和新的订单状态作为参数，更新订单表中指定订单的状态。
  - 计算订单总金额（CALCULATE_ORDER_TOTAL）：接收订单ID作为参数，计算指定订单的总金额并返回。
  - 获取客户的订单数量（GET_CUSTOMER_ORDER_COUNT）：接收客户ID作为参数，查询指定客户的订单数量并返回。
  
  ```
  CREATE OR REPLACE PACKAGE Sales_Package AS
  
  -- 存储过程：创建订单
  PROCEDURE Create_Order(p_customer_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER);
  
  -- 存储过程：取消订单
  PROCEDURE Cancel_Order(p_order_id IN NUMBER);
  
  -- 函数：计算订单总金额
  FUNCTION Calculate_Total_Amount(p_order_id IN NUMBER) RETURN NUMBER;
  
  END Sales_Package;
  
  
  CREATE OR REPLACE PACKAGE BODY Sales_Package AS
  
  PROCEDURE Create_Order(p_customer_id IN NUMBER, p_product_id IN NUMBER, p_quantity IN NUMBER) AS
  BEGIN
    -- 在订单表中插入订单数据
    INSERT INTO Orders (order_id, customer_id, order_date)
    VALUES (order_seq.NEXTVAL, p_customer_id, SYSDATE);
    
    -- 在订单详情表中插入订单商品的详细数据
    INSERT INTO Order_Details (order_id, product_id, quantity)
    VALUES (order_seq.CURRVAL, p_product_id, p_quantity);
  EXCEPTION
    WHEN OTHERS THEN
      -- 处理异常，回滚事务等
      NULL;
  END Create_Order;
  
  PROCEDURE Cancel_Order(p_order_id IN NUMBER) AS
  BEGIN
    -- 根据订单ID删除订单及订单详情
    DELETE FROM Orders WHERE order_id = p_order_id;
    DELETE FROM Order_Details WHERE order_id = p_order_id;
  EXCEPTION
    WHEN OTHERS THEN
      -- 处理异常，回滚事务等
      NULL;
  END Cancel_Order;
  
  FUNCTION Calculate_Total_Amount(p_order_id IN NUMBER) RETURN NUMBER AS
    total_amount NUMBER;
  BEGIN
    -- 查询订单详情表中指定订单的商品总金额
    SELECT
         SUM(quantity * price) INTO total_amount
         FROM Order_Details od
         JOIN Products p ON od.product_id = p.product_id
         WHERE od.order_id = p_order_id;
         
         RETURN total_amount;
       EXCEPTION
         WHEN NO_DATA_FOUND THEN
           RETURN 0; -- 如果订单不存在或者没有商品信息，则返回0
         WHEN OTHERS THEN
           -- 处理异常
           NULL;
  END Calculate_Total_Amount;
  END Sales_Package;
  ```
  
  ![2.jpg](./4.png)
  
- 第6步：生成模拟数据的代码

  ```
  -- 插入模拟数据到Customers表
  BEGIN
    FOR i IN 1..50000 LOOP
      INSERT INTO Customers (customer_id, name, contact)
      VALUES (i, 'Customer ' || i, 'contact ' || i);
    END LOOP;
    COMMIT;
  END;
  
  -- 插入模拟数据到Products表
  BEGIN
    FOR i IN 1..20000 LOOP
      INSERT INTO Products (product_id, name, price, quantity)
      VALUES (i, 'Product ' || i, TRUNC(DBMS_RANDOM.VALUE(1, 1000), 2), TRUNC(DBMS_RANDOM.VALUE(1, 100)));
    END LOOP;
    COMMIT;
  END;
  
  -- 插入模拟数据到Orders表和Order_Details表
  DECLARE
    v_customer_id NUMBER;
    v_product_id NUMBER;
  BEGIN
    FOR i IN 1..100000 LOOP
      -- 随机选择一个顾客和一个产品
      SELECT customer_id INTO v_customer_id FROM Customers WHERE ROWNUM = 1 ORDER BY DBMS_RANDOM.VALUE;
      SELECT product_id INTO v_product_id FROM Products WHERE ROWNUM = 1 ORDER BY DBMS_RANDOM.VALUE;
      
      -- 在Orders表中插入订单数据
      INSERT INTO Orders (order_id, customer_id, order_date)
      VALUES (i, v_customer_id, SYSDATE);
      
      -- 在Order_Details表中插入订单详情数据
      INSERT INTO Order_Details (order_id, product_id, quantity)
      VALUES (i, v_product_id, TRUNC(DBMS_RANDOM.VALUE(1, 10)));
    END LOOP;
    COMMIT;
  END;
  
  -- 插入模拟数据到Customers表
  BEGIN
    FOR i IN 50001..100000 LOOP
      INSERT INTO Customers (customer_id, customer_name, address)
      VALUES (i, 'Customer ' || i, 'Address ' || i);
    END LOOP;
    COMMIT;
  END;
  
  -- 插入模拟数据到Products表
  BEGIN
    FOR i IN 20001..40000 LOOP
      INSERT INTO Products (product_id, product_name, price, quantity)
      VALUES (i, 'Product ' || i, TRUNC(DBMS_RANDOM.VALUE(1, 1000), 2), TRUNC(DBMS_RANDOM.VALUE(1, 100)));
    END LOOP;
    COMMIT;
  END;
  
  -- 插入模拟数据到Orders表和Order_Details表
  DECLARE
    v_customer_id NUMBER;
    v_product_id NUMBER;
  BEGIN
    FOR i IN 100001..200000 LOOP
      -- 随机选择一个顾客和一个产品
      SELECT customer_id INTO v_customer_id FROM Customers WHERE ROWNUM = 1 ORDER BY DBMS_RANDOM.VALUE;
      SELECT product_id INTO v_product_id FROM Products WHERE ROWNUM = 1 ORDER BY DBMS_RANDOM.VALUE;
      
      -- 在Orders表中插入订单数据
      INSERT INTO Orders (order_id, customer_id, order_date)
      VALUES (i, v_customer_id, SYSDATE);
      
      -- 在Order_Details表中插入订单详情数据
      INSERT INTO Order_Details (order_id, product_id, quantity)
      VALUES (i, v_product_id, TRUNC(DBMS_RANDOM.VALUE(1, 10)));
    END LOOP;
    COMMIT;
  END;
  
  ```
  
  ![2.jpg](./5.png)
  
- 第5步：数据库备份方案

  - 定期备份：使用Oracle的备份工具，如RMAN（Recovery Manager），设置定期备份策略，包括完整备份和增量备份，并将备份文件存储在独立的存储设备上，以确保数据的安全性和可恢复性。
  - 存储介质：将备份数据存储在可靠的介质上，如磁带库、网络存储等，并根据需求进行存储介质的定期维护和更替。
  - 备份监控：设置备份监控系统，监控备份作业的状态和运行情况，及时发现和解决备份故障。
  - 日志备份：启用Oracle的归档日志模式，将数据库的归档日志备份到独立的存储设备上，以便在数据损坏或灾难恢复时使用。
  - 定点恢复测试：定期进行数据库备份恢复测试，以确保备份文件的有效性，并验证数据库恢复的可行性。

## 结论

学习完Oracle是一次非常有意义的经历。Oracle作为一种广泛应用的关系型数据库管理系统，它提供了丰富的功能和强大的性能，成为许多企业和组织的首选。

通过学习Oracle，我深刻认识到了数据库在现代信息管理中的重要性。Oracle的各种特性和工具使得数据的存储、检索和处理变得高效和可靠。我学会了使用SQL语言进行数据查询和操作，能够编写复杂的查询语句来满足不同的业务需求。

此外，Oracle还提供了高级功能，如事务管理、并发控制和数据备份恢复，这些功能在保证数据的一致性和可用性方面至关重要。学习Oracle使我更加了解了数据安全和数据完整性的重要性，以及如何应对数据库故障和灾难。

学习Oracle也让我认识到数据库管理的复杂性。从设计数据库模式到优化查询性能，都需要仔细考虑和细致的规划。我学会了设计和规范化数据库模式，以及优化查询计划来提升性能。

总的来说，学习Oracle让我对关系型数据库管理系统有了更深入的理解，提高了我的数据管理和处理能力。这将对我的职业发展产生积极的影响，使我在未来的工作中能够更好地应对各种数据管理和分析的挑战。


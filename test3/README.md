# 实验3：创建分区表

- 学号：202010211107     姓名：葛展鹏    班级：20软件工程1班

## 实验目的

掌握分区表的创建方法，掌握各种分区方式的使用场景。

## 实验内容

- 本实验使用实验2的sale用户创建两张表：订单表(orders)与订单详表(order_details)。
- 两个表通过列order_id建立主外键关联。给表orders.customer_name增加B_Tree索引。
- 新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。
- orders表按订单日期（order_date）设置范围分区。
- order_details表设置引用分区。
- 表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。
- 写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。
- 进行分区与不分区的对比实验。

## 实验步骤

- 第1步：创建订单表(orders)，并给orders表按订单日期（order_date）设置范围分区

  ![1.png](./1.png)

 - 第2步：创建订单表(order_details)，并给order_details表设置引用分区。

![2.jpg](./2.png)



- 第3步：给表orders.customer_name增加B_Tree索引

![5.png](./3.png)

- 第4步：新建两个序列，分别设置orders.order_id和order_details.id，插入数据的时候，不需要手工设置这两个ID值。

  ![6.png](./4.png)

  

- 第5步：表创建成功后，插入数据，数据应该能并平均分布到各个分区。orders表的数据都大于40万行，order_details表的数据大于200万行（每个订单对应5个order_details）。

  ![7.png](./5.png)

  ![8.png](./6.png)

- 第6步：写出插入数据的脚本和两个表的联合查询的语句，并分析语句的执行计划。

  

  ```
  SELECT o.order_id, o.customer_name, o.order_date, od.product_id, od.product_num, od.product_price
  FROM orders o
  JOIN order_details od
  ON o.order_id = od.order_id;
  ```

  

- 第7步：进行分区与不分区的对比实验。

  ![8.png](./6.png)

## 结论

本次实验涉及到Oracle数据库的分区和索引的使用。首先通过创建两张表，orders和order_details，建立主外键关联，给orders表的customer_name增加B-Tree索引。然后创建两个序列，分别设置orders.order_id和order_details.id，实现自动递增ID值的设置。orders表按订单日期设置范围分区，order_details表设置引用分区。最后通过插入数据的方式测试表的性能。在查询方面，使用两个表的联合查询，并分析查询计划，测试分区与不分区的性能差异。

通过本次实验，我深入了解了Oracle数据库的分区和索引的应用，了解到它们对于提高数据库性能的重要作用。同时，也深刻认识到在实际应用中，合理地使用分区和索引可以对提升数据库性能有很大的帮助。最后，我发现在数据量大的情况下，使用分区查询的性能明显优于不分区查询。

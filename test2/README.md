# 实验2：用户及权限管理

- 学号：202010211107     姓名：葛展鹏    班级：20软件工程1班

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验步骤

- 第1步：以system登录到pdborcl，创建角色con_res_role和用户sale，并授权和分配空间：

  ![1.png](./pict1.png)

 - 第2步：新用户sale连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

![2.jpg](./pict2.png)

![3.png](./pict3.png)

![4.png](./pict4.png)



- 第3步：用户hr连接到pdborcl，查询sale授予它的视图customers_view

![5.png](./pict5.png)

- 第4步：概要文件设置,用户最多登录时最多只能错误3次

  ![6.png](./pict6.png)

  

- 第5步：输入错误密码使sale用户锁定

  ![7.png](./pict7.png)

- 第6步：输入错误密码使sale用户锁定

  ![8.png](./pict8.png)

- 第7步：查看数据库的使用情况

  ![9.png](./pict9.png)

- 第8步：实验结束删除用户和角色

## 结论

1. 此次实验掌握了用户管理、角色管理、权根维护与分配的能力，我们在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
2. 掌握用户之间共享对象的操作技能，我们创建好视图以及表，此次实验中我们测试了一下用户hr,sale之间的表的共享，sale用户只共享了视图customers_view给hr,没有共享表customers。然而hr无法查询表customers。
3. 学习了概要文件对用户的限制，以及将已经锁定了的账户解锁。

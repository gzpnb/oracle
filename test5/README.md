# 实验5：包，过程，函数的用法

- 学号：202010211107     姓名：葛展鹏    班级：20软件工程1班

## 实验目的

了解PL/SQL语言结构

了解PL/SQL变量和常量的声明和使用方法

学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录
  1. 创建一个包(Package)，包名是MyPack。
  2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
  3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。 Oracle递归查询的语句格式是：

## 实验步骤

- 第1步：创建一个包(Package)，包名是MyPack。

  ```
  create or replace PACKAGE MyPack IS
  /*
  本实验以实验4为基础。
  包MyPack中有：
  一个函数:Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)，
  一个过程:Get_Employees(V_EMPLOYEE_ID NUMBER)
  */
  FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
  PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
  END MyPack;
  /
  create or replace PACKAGE BODY MyPack IS
  FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
  AS
      N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
      BEGIN
      SELECT SUM(salary) into N  FROM EMPLOYEES E
      WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
      RETURN N;
      END;
  
  PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
  AS
      LEFTSPACE VARCHAR(2000);
      begin
      --通过LEVEL判断递归的级别
      LEFTSPACE:=' ';
      --使用游标
      for v in
          (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
          START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
          CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
      LOOP
          DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                              V.EMPLOYEE_ID||' '||v.FIRST_NAME);
      END LOOP;
      END;
  END MyPack;
  /
  ```

  ![2.jpg](./1.png)

 - 第2步：在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。

   ```
   select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;
   ```

   

![2.jpg](./2.png)

- 第3步：在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。

  ```
  set serveroutput on
  DECLARE
  V_EMPLOYEE_ID NUMBER;    
  BEGIN
  V_EMPLOYEE_ID := 101;
  MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
  END;
  
  ```

  ![2.jpg](./3.png)

## 结论

本次实验要求我们在 Oracle 数据库中创建一个包 MyPack，通过在包中创建函数和过程来实现部门工资总额统计和递归查询员工的功能。

首先，在 MyPack 包中创建函数 Get_SalaryAmount，通过输入部门 ID，查询员工表并统计该部门的 salary 工资总额。可以通过编写 SQL 语句来实现该函数的功能，具体可以通过使用 SUM 函数进行统计，以及使用 GROUP BY 子句对部门进行分组。

其次，在 MyPack 包中创建过程 GET_EMPLOYEES，通过输入员工 ID，使用游标递归查询某个员工及其所有下属，子下属员工。Oracle 递归查询可以使用 WITH 子句和 CONNECT BY 子句来实现，具体可以参考 Oracle 官方文档和相关书籍进行学习和实践。

通过本次实验，我深入了解了 Oracle 数据库中包的概念和使用方法，掌握了创建函数和过程的基本步骤和语法规则。同时，我也加深了对 SQL 语句的理解和应用能力，学习了如何使用游标和递归查询实现复杂的数据操作和分析。

在实验过程中，我遇到了一些问题，例如语法错误和查询结果不符等，但通过查找资料和请教老师，最终成功解决了这些问题。

总之，本次实验让我掌握了 Oracle 数据库中包的基础知识和使用方法，了解了函数和过程的语法规则和应用场景。

